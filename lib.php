<?php
/**
 * *************************************************************************
 * *                             events                                 **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  events                                                  **
 * @name        events                                                  **
 * @copyright   Glendon ITS                                               **
 * @link        http://www.glendon.yorku.ca/its                           **
 * @author      Patrick Thibaudeau                                        **
 * @author      Johanna Parrales                                          **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
require_once(dirname(__FILE__) . '../../../config.php');

function local_events_cron() {
    global $CFG, $DB;

    return true;
}

/**
 * Update the navigation block with events options
 * @global moodle_database $DB
 * @global stdClass $USER
 * @global stdClass $CFG
 * @global moodle_page $PAGE
 * @param global_navigation $navigation The navigation block
 */
function local_events_extend_navigation(global_navigation $navigation) {
    global $DB, $USER;

    //Only display if panorama is installed
    $pr_config = $DB->count_records('config_plugins',
            array('plugin' => 'local_events'));
    if ($pr_config > 0) {
        $node = $navigation->find('local_events',
                navigation_node::TYPE_CONTAINER);
        if (!$node) {
            $node = $navigation->add(get_string('pluginname', 'local_events'),
                    null, navigation_node::TYPE_CONTAINER,
                    get_string('pluginname', 'local_events'), 'local_events');
            $node->showinflatnavigation = true;
        }

        $context = context_system::instance();
        //The user can see that IF he has rights on at least one category
        //profile 4 is the READ ONLY. we don't use the local_events::PROFILE_READONLY because it is not loaded here

        $node->add(get_string('pluginname', 'local_events'),
                new moodle_url('/local/events/index.php'));
    }
}

function local_events_pluginfile($course, $cm, $context, $filearea, $args,
        $forcedownload, array $options = array()) {
    global $DB;

    if ($context->contextlevel != CONTEXT_COURSE) {
        return false;
    }

    require_login();

    if ($filearea != array(
        'interest',
        'cv',
        'transcripts',
        'reference',
        'administrative',
        'risk',
        'memorandum',
        'passport',
        'reference_letters',
        'language_assessement',
        'study_plan',
        'budget',
        'payment',
        'private_notes',
        'course_assessment',
        'osap_info',
        'host_letter',
        'york_letter'
            )) {
        return false;
    }

    $itemid = (int) array_shift($args);


    $fs = get_file_storage();
    $filename = array_pop($args);

    if (empty($args)) {
        $filepath = '/';
    } else {
        $filepath = '/' . implode('/', $args) . '/';
    }

    $file = $fs->get_file($context->id, 'local_events', $filearea, $itemid,
            $filepath, $filename);
    if (!$file) {
        return false;
    }

    send_stored_file($file, 0, 0, $forcedownload, $options);
}
