<?php

$string["action"] = "Action";
$string["cancel"] = "Annuler";
$string["click_course_help"] = "Cliquez sur l'un de vos cours ci-dessous pour voir la liste des étudiants.";
$string["confirm_delete"] = "Êtes-vous sûr de vouloir supprimer ce dossier? Cette action ne peut pas être annulée.";
$string["contenten"] = "Corps du message";
$string["contentfr"] = "Message";
$string["deadline"] = "Date limite";
$string["deadlines"] = "Dates limites";
$string["delete"] = "Supprimer";
$string["dev_mode"] = "Developer mode";
$string["dev_mode_email"] = "Developer mode email";
$string["dev_mode_email_help"] = "Enter the email address to test sent emails";
$string["drop"] = "Abandonner";
$string["dropdate"] = "Date";
$string["edit_deadlines"] = "Modifier les dates limites";
$string["edit_messages"] = "Modifier les messages";
$string["english_message"] = "Message en anglais";
$string["fall"] = "Automne";
$string["for_course"] = " pour le cours: ";
$string["french_message"] = "Message en français";
$string["last_modified_by"] = "Modifier par";
$string["list_course_selection"] = "Sélectionnez un cours:";
$string["list_of_students"] = "Liste des étudiants ";
$string["log_report"] = "Afficher le rapport de journal";
$string["manual_course_selection"] = "Ou chercher un cours:";
$string["message"] = "Message";
$string["messages"] = "Messages";
$string["modified_on"] = "Modifier le";
$string["events:admin"] = "Mid-term Progress Report Tool Administrator ";
$string["name"] = "Nom du message";
$string["period"] = "Période";
$string["pluginname"] = "Outil de rapport de mi-session";
$string["required"] = "Ce champ est requis.";
$string["send"] = "Envoyer un courriel";
$string["send_again"] = "Envoyer de nouveau";
$string["send_email_help"] = "Cliquez sur le lien \"Envoyer un courriel\" pour envoyer un courriel de rapport de mi-session aux étudiants dont la moyenne est inférieure à un D+ dans votre cours.";
$string["sent"] = "Courriel envoyé";
$string["subjecten"] = "Subject";
$string["subjectfr"] = "Sujet";
$string["winter"] = "Hiver";
$string["withdraw"] = "Retrait";
$string["withdrawaldate"] = "Date de retrait";
$string["year"] = "Année";

/*LDAP Setting*/
$string['ldap_url'] = 'LDAP Url';
$string['ldap_user'] = 'LDAP User';
$string['ldap_password'] = 'LDAP User Password';

/*Date formatting*/
$string['strftimedate'] = '%d %B, %Y';