<?php
/**
 * *************************************************************************
 * *                             events                                 **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  events                                                  **
 * @name        events                                                  **
 * @copyright   Glendon ITS                                               **
 * @link        http://www.glendon.yorku.ca/its                           **
 * @author      Patrick Thibaudeau                                        **
 * @author      Johanna Parrales                                          **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */

//Page 1, 2 , 3
$string["events:addinstance"] = "events Add instance";
$string["events:admin"] = "events administrator";
$string["activity_level"] = "Activity Level / Niveau d’activité";
$string["address"] = "Street Address / Adresse";
$string["application_date"] = "Application Date / Date de la demande";
$string["application_form_for"] = "Viewing application form for:";
$string["basis"] = "Basis / Base";
$string["budget"] = "Budget / Budget";
$string["campus"] = "Your Campus / Votre campus";
$string["cellphone"] = "Cellphone / Cellulaire";
$string["choice1"] = "Choice 1 / Choix 1";
$string["choice2"] = "Choice 2 / Choix 2";
$string["choice3"] = "Choice 3 / Choix 3";
$string["citizenship"] = "Citizenship / Citoyenneté";
$string["city"] = "City / Ville";
$string["cob"] = "Country of Birth / Pays de naissance";
$string["coming_going"] = "Are you coming on events or going on events? / Venez-vous en échange ou partez-vous en échange ?";
$string["coming"] = "Coming on events / Venir en échange "
        . "<br>" 
        . "(only for students who are not from York University / pour les étudiants qui ne sont pas de l’Université York)";
$string["continue"] = "Continue";
$string["continue_to_change_form"] = "You have already started to fill out the form. Press next to continue.";
$string["correspondence"] = "Language of correspondence / Langue de correspondance";
$string["country"] = "Country / Pays";
$string["date_received"] = "Date received / Date de réception";
$string["dob"] = "Date of Birth / Date de naissance";
$string["email"] = "Email / Courriel";
$string["export"] = "Export";
$string["fall"] = "1) Fall / Automne";
$string["female"] = "Female / Femme";
$string["first_choice_university"] = "If yes, please indicate the university you have selected as your first choice / Si oui, quelle est la première université que vous avez choisie?";
$string["first_language"] = "First language / Langue maternelle";
$string["firstname"] = "First name / Prénom";
$string["gender"] = "Gender / Genre";
$string["going"] = "Going on events / Partir en échange "
        . "<br>"
        ."(only for York University students)";
$string["home_campus"] = "Your Home Campus / Votre campus actuel";
$string["home_telephone"] = "Home Telephone / Téléphone maison";
$string["home_university"] = "Home University Name (in your country) / Nom de l’université (dans votre pays)";
$string["language_assessement"] = "Language Assessement / Évaluation de langue";
$string["lastname"] = "Name / Nom ";
$string["male"] = "Male / Homme";
$string["next"] = "Next / Suivant";
$string["no_record_found"] = "No application found for this user";
$string["not_yet_submitted"] = "Not yet submitted";
$string["paid_fees"] = "Did you pay your $100 application fees to York University? / Avez-vous payé les frais de candidature de 100$ a York ?";
$string["paid_yes"] = "Yes / Oui";
$string["paid_no"] = "No / Non";
$string["planned_field_of_study"] = "Choose Your Host University  / Choissisez votre université d'accueil :"
        . "<br>"
        . "(Choose at least one / Choisissez-en un)";
$string["pluginname"] = "events Application Form";
$string["postal_zip"] = "Postal / Zip code / Code postal";
$string["previous"] = "Previous / Précédent";
$string["previous_sisid"] = "Previous SIS ID / ID SIS précédent";
$string["privacy"] = "Protection de la vie privée: Les renseignements personnels fournis dans ce "
        . "formulaire sont recueillis en vertu de la loi The York University Act, 1965 et seront "
        . "utilisés à des fins éducatives, administratives et statistiques. Si vous avez des "
        . "questions concernant la collecte, l’utilisation et la divulgation, par l’Université "
        . "York, de ces renseignements personnels, veuillez contacter Information, Privacy and "
        . "Copyright Office, 1050 Kaneff Tower, York University, 4700 Keele St. Toronto ON Canada "
        . "M3J 1P3. Phone: (416) 736-2100 x 40706 (Messagerie) ou par courriel à "
        . "<a href='mailto:info.privacy@yorku.ca'>info.privacy@yorku.ca</a>."
        . "<br>"
        . "<br>"
        . "Privacy:  Personal information in connection with this form is collected under the "
        . "authority of The York University Act, 1965 and will be used for educational, "
        . "administrative and statistical purposes. If you have any questions about the collection, "
        . "use and disclosure of personal information by York University, please contact: Information, "
        . "Privacy and Copyright Office, 1050 Kaneff Tower, York University, 4700 Keele St. Toronto, "
        . "ON Canada M3J 1P3. Phone: (416) 736-2100 x 40706 (Voicemail) or by email at "
        . "<a href='mailto:info.privacy@yorku.ca'>info.privacy@yorku.ca</a>.";
$string["private_notes"] = "Private notes";
$string["province"] = "Canada/USA - Prov/State / Canada/États-Unis - Prov/état";
$string["required"] = "This field is required / Ce champ est requis";
$string["salutation"] = "Salutation / Salutation";
$string["search_student"] = "Search for a student using the name or SISID";
$string["second_name"] = "Second name / Second nom";
$string["select"] = "Select / Selectionner";
$string["study_plan"] = "Study Plan / Plan d’études";
$string["submit"] = "Submit / Soumettre";
$string["summer"] = "3) Summer / Été";
$string["timesubmitted"] = "Date submitted<br>(dd-mm-yyyy)";
$string["yu_events_code"] = "YU events Code / Code d’échange UY";
$string["yu_events_code_help"] = "Code of your home University / code de l’Université de votre pays";

$string['average_gpa'] = "Average GPA / Moyenne cumulative";
$string['current_minor'] = "Your current Minor / Votre Mineure";
$string['current_year_study'] = "Current year of study / Année d’études actuelle";

$string["Miss"] = "Miss";
$string["Mr"] = "Mr";
$string["Mrs"] = "Mrs";
$string["Ms"] = "Ms";
$string["Reverend"] = "Reverend";
$string["Captain"] = "Captain";
$string["Dr"] = "Dr";
$string["Fr"] = "Fr";
$string["M"] = "M";
$string["Mlle"] = "Mlle";
$string["Mme"] = "Mme";
$string["Sr"] = "Sr";
$string["Rabbi"] = "Rabbi";
$string["Brother"] = "Brother";
$string["Colonel"] = "Colonel";
$string["General"] = "General";
$string["Major"] = "Major";
$string["Lieutenant"] = "Lieutenant";
$string["Admiral"] = "Admiral";
$string["Sargeant"] = "Sargeant";
$string["Professor"] = "Professor";
$string["Corporal"] = "Corporal";
$string["Sir"] = "Sir";


$string["other"] = "Other / Autre";
$string["sisid"] = "Student number / Numéro d'Étudiant";
$string["specialization_major"] = "Your current specialization and/or Major / Votre spécialisation et/ou Majeure";
$string["start_year"] = "Start Year / Année de démarrage";
$string["start_term"] = "Start Term / Démarrage de la session";
$string["state"] = "U.S. State";
$string["state_province_region"] = "International Province / State – Province, Département ou région à l’international";
$string["status"] = "Status in Canada / Statut au Canada";
$string["study_level"] = "Study Level / Niveau d'étude";
$string["study_period"] = "Study Period / Période d’études";
$string["university_country"] = "University Country / Pays de l'université";
$string["winter"] = "2) Winter / Hiver";
$string["yip_yes"] = "Yes / Oui";
$string["yip_no"] = "No / Non";
$string["york_international_program"] = "Have you applied for the York International Program? / Avez-vous fait une demande pour le programme d’échanges avec York International ?";


//Page 4
//$string["choose_level"] = "Please choose which best describes your level in French, English, and Spanish / Choisir ce qui décrit le mieux ton niveau en français, anglais et espagnol";

$string["reading_in_french"] = "Reading in French / Lecture en français";
$string["writing_in_french"] = "Writing in French / Écriture en français";
$string["speaking_in_french"] = "Speaking in French / Compétence orale en français";
$string["comprehension_in_french"] = "Comprehension in French / Compréhension en français";

$string["reading_in_english"] = "Reading in English / Lecture en anglais";
$string["writing_in_english"] = "Writing in English / Écriture en anglais";
$string["speaking_in_english"] = "Speaking in English / Compétence orale en anglais";
$string["comprehension_in_english"] = "Comprehension in English / Compréhension en anglais";

$string["reading_in_spanish"] = "Reading in Spanish / Lecture en espagnol";
$string["writing_in_spanish"] = "Writing in Spanish / Écriture en espagnol";
$string["speaking_in_spanish"] = "Speaking in Spanish / Compétence orale en espagnol";
$string["comprehension_in_spanish"] = "Comprehension in Spanish / Compréhension en espagnol";

$string["language_training"] = "If applicable, please describe your formal and informal training in Spanish, in English or in French / Si applicable, décrivez votre formation formelle et informelle en espagnol, en anglais ou en français";

//Page Files
$string['administrative_documents'] = "Administrative Documents / Documents Administratifs ";
$string['cv'] = "Resume / CV";
$string['interest'] = "Statement of Interest / Lettre de motivation";
$string['memorandum'] = "Acceptance Letter / Lettre d’admission";
$string['passport'] = "Image of Passport (First page) / Image du Passeport (Première page)";
$string['payment'] = "Proof of Payment / Preuve de paiement";
$string['transscripts'] = "Transcripts / Relevé de notes";
$string['reference_letter'] = "Reference letter / Lettre de référence";
$string['risk_waiver'] = "events Program Academic Decision / Décision Académique Programme d’échanges";
$string['course_assessment'] = "Course assessment form/Formulaire d’évaluation des cours";
$string['osap_info'] = "OSAP Supplemental Information/ Renseignements supplémentaires RAFEO";
$string['host_letter'] = "Host University Acceptance Letter / Lettre d’admission de l’Université d’accueil";
$string['york_letter'] = "York University Acceptance Letter / Lettre d’admission de l’Université York";

// Page Emergency contact
$string['emergency_heading'] = "Emergency Contact Form / Formulaire de contact d'urgence";
$string['passport_number'] = "Passport Number / Numéro de passeport";
$string['international_program'] = "International Program / Programme international";
$string['start_date_program'] = "Start Date of Program / Date de début du programme";
$string['end_date_program'] = "End Date of Program / Date de fin du programme";
$string['host_partner_institution'] = "Name of Host or Partner Institution / Nom de l'institution d'accueil ou partenaire";
$string['host_partner_city'] = "City / Ville";
$string['host_partner_country'] = "Country / Pays";
$string['accommodation_address'] = "Accommodation address during education abroad / Adresse du logement pendant votre séjour à l'étranger";
$string['email_away'] = "Email while away / Adresse courriel pendant votre séjour à l'étranger";
$string['insurance_coverage'] = "Insurance Coverage / Assurance";
$string['insurance_provider'] = "Insurance Provider / Nom de Assureur"
        . "<br>"
        . "Please note that Guard.me insurance is mandatory/ Merci de noter que l'assurance Guard.me est obligatoire";
$string['insurance_policy_number'] = "Insurance Policy Number / Numéro de la police d'assurance";
$string['checked_embassy'] = "I have checked the contact information for my home country's "
        . "Embassy/Consulate closest to my host institution abroad. / "
        . "<br>"
        . "J'ai vérifié les coordonnées de l'ambassade/consulat de mon pays d'origine le plus "
        . "proche de mon institution d'accueil.";
$string['canadian_citizenship'] = "I have Canadian citizenship / J'ai la citoyenneté canadienne";            
$string['registered_contact'] = "I have registered my contact details through the "
        . "<a href='https://travel.gc.ca/travelling/registration?_ga=2.209573521.209695030.1551204585-398714178.1551109203'>"
        . "Registration of Canadians Abroad System</a>. / "
        . "<br>"
        . "J'ai inscrit mes coordonnées sur le site des "
        . "<a href='https://voyage.gc.ca/voyager/inscription?_ga=2.69542928.1688413952.1552407965-752406103.1552407965'>"
        . "Inscriptions des Canadiens à l’étranger</a>.";            
$string['emergency_name'] = "Emergency Contact Name / Nom de votre contact en cas d'urgence"; 
$string['emergency_relationship'] = "Relationship / Relation"; 
$string['emergency_email'] = "Emergency Contact Email Address / Adresse courriel de votre contact en cas d'urgence"; 
$string['emergency_address'] = "Emergency Contact Street Number and Address / Numéro de rue et adresse de votre contact en cas d'urgence"; 
$string['emergency_city'] = "Emergency Contact City / Ville de votre contact en cas d'urgence"; 
$string['emergency_province'] = "Emergency Contact Province / Province de votre contact en cas d'urgence"; 
$string['emergency_postal_code'] = "Emergency Contact Postal Code / Code postal de votre contact en cas d'urgence"; 
$string['emergency_home_number'] = "Emergency Contact Home Phone Number / Numéro de téléphone à la maison de votre contact en cas d'urgence"; 
$string['emergency_confirmation'] = "<div class='card'  style=\"width: 100%;\"> <div class='card-body'>"
        . "<div class=\"alert alert-info\">Confirmation - I have informed my Emergency Contact(s) about this designation "
        . "and regarding all aspects of my proposed abroad program including the nature of any possible risks. "
        . "<br>"
        . "<span style=\"color: red;\">(Enter your first and last name as signature confirmation)</span> / "
        . "<br>"
        . "Confirmation - J'ai informé mon/mes contact/s au sujet de ce formulaire d’urgence et concernant mon "
        . "programme d'échange à l’étranger ainsi que tous les risques possibles. "
        . "<br>"
        . "<span style=\"color: red;\">(Indiquez votre prénom et nom pour confirmer pour signer)</span></div> </div> </div>"; 
$string['emergency_confirmation_name'] = "Emergency Contact Name / Nom du contact d'urgence"; 
$string['emergency_confirmation_date'] = "<div class='card'  style=\"width: 100%;\"> <div class='card-body'>"
        . "<div class=\"alert alert-info\">Confirmation Date / Date de confirmation </div></div></div>"; 


// Page Waiver
$string['waiver_heading'] = "Waiver form (English only) / Formulaire de renonciation (anglais seulement)"; 
$string['course_name'] = "Course Name (For events enter events)"; 
$string['professor_name'] = "Professor's Name (For events enter events)"; 
$string['assumption_risk'] = "<div class='card'  style=\"width: 100%;\"> <div class='card-body'>"
        . "<div class=\"alert alert-info\">ASSUMPTION OF RISKS : My participation in the "
        . "Program will take me away from campus for an extended period of time. "
        . "During this period, I will be in an unfamiliar surrounding and exposed "
        . "to risks to my person and possessions. These risks include the "
        . "possibility of loss, illness, injury and even death as a result of or "
        . "caused by accidents, crime, violence, and civil unrest, and I may also "
        . "feel lonely or homesick at times. I understand that York University is "
        . "not able to ensure my safety or control my exposure to such risks, "
        . "dangers and hazards. I freely and voluntarily accept and assume all "
        . "such risks, dangers and hazards.</div></div></div>"; 
$string['assumption_responsibility'] = "<div class='card'  style=\"width: 100%;\"> <div class='card-body'>"
        . "<div class=\"alert alert-info\">ASSUMPTION OF RESPONSIBILITY: I shall "
        . "abide by the Program’s rules and all applicable policies, procedures, "
        . "guidelines, rules, regulations and codes governing student conduct of "
        . "each of York University and the host institution, as well as the laws "
        . "of the host country. I shall ensure that I have made appropriate travel "
        . "arrangements and I have adequate and sufficient medical, personal health, "
        . "dental and accident insurance coverage, as well as protection and coverage "
        . "for my personal possessions, for the duration of the Program. York University "
        . "does not carry accident or injury insurance for my benefit and there "
        . "may be matters for which I could be held financially responsible, for "
        . "my own expenses or losses of those of other persons, particularly if "
        . "I am at fault personally in the cause of such expenses or losses. "
        . "I am accountable in all respects for my own actions, omissions and "
        . "negligence, and I shall not ask York University, its Board of Governors, "
        . "officers, employees, and agents to assume or accept the consequences "
        . "thereof. Further, I am responsible for any claims made by third parties "
        . "against York University, its Board of Governors, officers, employees, "
        . "and agents in relation to such actions, omissions and negligence. "
        . "I have been advised by York University of such risks, dangers and "
        . "hazards as well as the need to act in a responsible and reasonable "
        . "manner at all times. York University does not supervise any of the "
        . "host institution academic programs, living arrangements, or extracurricular "
        . "activities during my participation in the Program. Furthermore, "
        . "York University ceases to be my sponsor should I elect to remain "
        . "abroad at the location of the Program or go elsewhere after the Program. "
        . "My electronic signature below is given freely in order to indicate my "
        . "understanding and acceptance of these realities and my assumption of "
        . "responsibility.</div></div></div> ";
$string['waiver'] = "<div class='card'  style=\"width: 100%;\"> <div class='card-body'>"
        . "<div class=\"alert alert-info\">RELEASE, WAIVER AND INDEMNITY: I hereby release York University, "
        . "its Board of Governors, officers, employees, agents, successors and "
        . "assigns (the “Released Parties”) from any and all losses, liabilities, "
        . "damages, injuries including death, and any other liability of any kind "
        . "including negligence, howsoever arising out of or in connection with "
        . "my participation in the Program or travel abroad, and I hereby waive "
        . "all claims, demands, lawsuits, costs, and expenses I may incur including "
        . "legal fees and disbursements. I shall indemnify and hold harmless the "
        . "Released Parties from any and all losses, liabilities, damages, injuries, "
        . "claims, demands, lawsuits, costs, expenses including legal fees and "
        . "disbursements, and any other liability of any kind including negligence, "
        . "howsoever arising out of or in connection with my participation in "
        . "the Program or travel abroad. This Agreement is governed by the laws "
        . "of the Province of Ontario and federal laws of Canada applicable therein. "
        . "This Agreement survives termination of my participation in the Program. "
        . "This Agreement cannot be modified or interpreted except in writing by "
        . "York University and no oral modification or interpretation is valid. "
        . "This Agreement enures to the benefit of and is binding upon me, my heirs, "
        . "next of kin, executors, administrators, representatives, successors and assigns.</div></div></div>"; 
$string['departure_date'] = "Date of Departure "; 
$string['waiver_signature'] = "As Signature confirmation, enter your first name and last name / "
        . "Pour confirmer votre signature, entrez votre prénom et votre nom de famille."; 
$string['waiver_signature_date'] = "Date of Signature: "; 

//Page 5
$string["legal_agreement"] = "J’autorise l’Université York, Glendon à transmettre la présente demande "
        . "et les pièces jointes requises à l’université d’accueil qui étudiera mon dossier. "
        . "Je m’engage à respecter la réglementation en vigueur dans les universités et leurs décisions. "
        . "Je déclare que les renseignements ci-dessus sont complets et exacts et j’ai bien pris "
        . "soin de lire et de comprendre les conditions d’admissibilité et de participation "
        . "au programme d’échanges de Glendon."
        . "<br>"
        . "<br>"
        . "I authorize York University, Glendon to forward this application and the required "
        . "attachments to the host university that will review my file. I agree to abide by the "
        . "regulations and decisions of the host university. I declare that the above information is "
        . "complete and accurate. I have taken good care to read and understand the eligibility and "
        . "participation requirements of the Glendon events Program.";
$string["legal_yes"] = "Yes / Oui";
$string["legal_no"] = "No / Non";

//Confirmation page
$string["confirmation"] = "Your form has been submitted. / Votre formulaire a été soumis.";

//Admin landing page
$string["coming_events"] = " Coming on events / Venir en échange &nbsp; ";
$string["going_events"] = " Going on events / Partir en échange";

// Results page
$string["for_administration"] = "Adminstration Purposes Only";
$string["section1"] = "Section 1: Page One";
$string["section2"] = "Section 2: Page Two";
$string["section3"] = "Section 3: Page Three";
$string["section4"] = "Section 4: Page Four";
$string["section5"] = "Section 5: Page Files";
$string["section6"] = "Section 6: Emergency Contact Page";
$string["section7"] = "Section 7: Waiver Page";
$string["section8"] = "Section 8: Page Five";
