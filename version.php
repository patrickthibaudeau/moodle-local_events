<?php
/**
 * *************************************************************************
 * *                             events                                 **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  events                                                  **
 * @name        events                                                  **
 * @copyright   Glendon ITS                                               **
 * @link        http://www.glendon.yorku.ca/its                           **
 * @author      Patrick Thibaudeau                                        **
 * @author      Johanna Parrales                                          **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */

$plugin->version = 2019080202;
// Moodle version required
$plugin->requires = 2010112400;
$plugin->maturity = MATURITY_STABLE;
$plugin->release = '1.0.3 (Build 2019062700)';
$plugin->cron = 0;
$plugin->component = 'local_events';
