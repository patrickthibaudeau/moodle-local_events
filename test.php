<?php
/**
 * *************************************************************************
 * *                              events                               **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  events                                                 **
 * @name        events                                                 **
 * @copyright   oohoo.biz                                                 **
 * @link        http://oohoo.biz                                          **
 * @author                                                                **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
require_once('config.php');


echo __DIR__  . '/../../lib/behat/behat_base.php';
die;

/**
 * Display the content of the page
 * @global stdobject $CFG
 * @global moodle_database $DB
 * @global core_renderer $OUTPUT
 * @global moodle_page $PAGE
 * @global stdobject $SESSION
 * @global stdobject $USER
 */
function display_page() {
    // CHECK And PREPARE DATA
    global $CFG, $OUTPUT, $SESSION, $PAGE, $DB, $COURSE, $USER;

    $id = optional_param('id', 0, PARAM_INT); //List id

    require_login(1, false); //Use course 1 because this has nothing to do with an actual course, just like course 1

    $context = context_system::instance();

    $pagetitle = get_string('pluginname', 'local_events');
    $pageheading = get_string('pluginname', 'local_events');


    echo \local_events\Base::page('/test.php?id=' . $id, $pagetitle, $pageheading, $context);

    $HTMLcontent = '';
    //**********************
    //*** DISPLAY HEADER ***
    //**********************
    echo $OUTPUT->header();
//    $initjs = "$(document).ready(function() {
//                   init_pluginname();
//               });";
//
//    echo html_writer::script($initjs);


    //**********************
    //*** DISPLAY CONTENT **
    //**********************
    $Object = new \local_events\Notifications();
    ?>
    <div id="events_wrapper">
        <div class='container'>
            <div class='row'>
                <div class='col-md-12'>
                    <div class='panel panel-default'>
                        <div class="panel-heading"><h3><i class='fa fa-gear'></i> Panel 1</h3></div>
                        <div class='panel-body'>
                            <?php 
                            echo $CFG->events_devmode;
//                            echo $Object->getMessage();
                            ?>
                        </div>
                    </div>
                </div>
               
            </div>
           
        </div>
    </div>

    <?php
    //**********************
    //*** DISPLAY FOOTER ***
    //**********************
    echo $OUTPUT->footer();
}

display_page();
?>
