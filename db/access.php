<?php
/**
 * *************************************************************************
 * *                             events                                 **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  events                                                  **
 * @name        events                                                  **
 * @copyright   Glendon ITS                                               **
 * @link        http://www.glendon.yorku.ca/its                           **
 * @author      Patrick Thibaudeau                                        **
 * @author      Johanna Parrales                                          **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */

$capabilities = array(

    'local/events:addinstance' => array(
        'riskbitmask' => RISK_SPAM | RISK_XSS,
 
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'editingteacher' => CAP_ALLOW,
            'manager' => CAP_ALLOW
        ),
 
    ),
    
    'local/events:admin' => array(
        'riskbitmask' => RISK_SPAM | RISK_XSS,
 
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
        ),
 
    ),
 
);