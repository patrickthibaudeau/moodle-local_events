<?php

/**
 * *************************************************************************
 * *                       Kiosk Web Services                             **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  kioskws                                                   **
 * @name        kioskws                                                   **
 * @copyright   Glendon ITS                                               **
 * @link        http://www.glendon.yorku.ca/its                           **
 * @author      Patrick Thibaudeau                                        **
 * @author      Glendon ITS                                               **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */

// We defined the web service functions to install.

$functions = array(
    'kiosk_create_event' => array(
        'classname' => 'local_events_external',
        'methodname' => 'create_event',
        'classpath' => 'local/events/externallib.php',
        'description' => 'creates an event',
        'type' => 'write',
         'capabilities' => ''
    ),
);

//'C: Creates an event request, creates Google Calendar event using google calendar api.'
//        . '               R: User is returned with a success or error message'
//        . '               U: Soft delete/archive. Acceptor role can edit existing requests (Maybe)'
 //       . '               D: Soft delete'