<?php

/**
 * *************************************************************************
 * *                             events                                 **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  events                                                  **
 * @name        events                                                  **
 * @copyright   Glendon ITS                                               **
 * @link        http://www.glendon.yorku.ca/its                           **
 * @author      Patrick Thibaudeau                                        **
 * @author      Johanna Parrales                                          **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */
defined('MOODLE_INTERNAL') || die();

function xmldb_local_events_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();
    if ($oldversion < 2019072301) {

        // Define table kiosk_events to be created.
        $table = new xmldb_table('kiosk_events');

        // Adding fields to table kiosk_events.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('email', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL, null, null);
        $table->add_field('title', XMLDB_TYPE_CHAR, '1000', null, XMLDB_NOTNULL, null, null);
        $table->add_field('eventdate', XMLDB_TYPE_INTEGER, '20', null, XMLDB_NOTNULL, null, null);
        $table->add_field('starttime', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, null);
        $table->add_field('location', XMLDB_TYPE_CHAR, '1000', null, XMLDB_NOTNULL, null, null);
        $table->add_field('reservedroom', XMLDB_TYPE_BINARY, null, null, XMLDB_NOTNULL, null, null);
        $table->add_field('budgetnumber', XMLDB_TYPE_INTEGER, '20', null, XMLDB_NOTNULL, null, null);
        $table->add_field('maxattendance', XMLDB_TYPE_INTEGER, '4', null, XMLDB_NOTNULL, null, null);
        $table->add_field('description', XMLDB_TYPE_TEXT, null, null, null, null, null);

        // Adding keys to table kiosk_events.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, ['id']);

        // Conditionally launch create table for kiosk_events.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Events savepoint reached.
        upgrade_plugin_savepoint(true, 2019072301, 'local', 'events');
    }       
    //Update 2
    if ($oldversion < 2019080102) {

        // Define field id to be added to kiosk_events.
        $table = new xmldb_table('kiosk_events');
        $field = new xmldb_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null, null);

        // Conditionally launch add field id.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Events savepoint reached.
        upgrade_plugin_savepoint(true, 2019080102, 'local', 'events');
    }
    //Update 3
        if ($oldversion < 2019080201) {

        // Define field id to be added to kiosk_events.
        $table = new xmldb_table('kiosk_events');
        $field = new xmldb_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null, null);

        // Conditionally launch add field id.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Events savepoint reached.
        upgrade_plugin_savepoint(true, XXXXXXXXXX, 'local', 'events');
    }



    
    return true;
}
