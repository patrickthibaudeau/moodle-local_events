<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace local_events;

/**
 * Description of event
 *
 * @author karl
 */
class event {
    //put your code here
    
    private $id;
    
    private $name;
    
    private $email;
    
    private $eventtitle;
    
    private $eventdate;
    
    private $starttime;
    
    private $location;
    
    private $reservedroom;
    
    private $budgetnumber;
    
    private $maxattendance;
    
    private $eventdescription;
    
    private $endtime;
    
    private $deleted;
    
    
    public function _construct($data){
        $this->name = $data['name'] ?? '';
        $this->email = $data['email'] ?? '';
        $this->eventtitle = $data['eventtitle'] ?? '';
        $this->eventdate = $data['eventdate'] ?? '';
        $this->starttime = $data['starttime'] ?? '';
        $this->location = $data['location'] ?? '';
        $this->reservedroom = $data['reservedroom'] ?? '';
        $this->budgetnumber = $data['budgetnumber'] ?? '';
        $this->maxattendance = $data['maxattendance'] ?? '';
        $this->eventdescription = $data['eventdescription'] ?? '';
        $this->endtime = $data['endtime'] ?? '';
        $this->deleted = $data['deleted'] ?? 0;
    }
    /**
     * 
     * @global \moodle_database $DB
     * @param type $data
     */
    public function create_event($data){
        global $DB;
        
        $eventID = $DB->insert_record('kiosk_events',$data);
        return $eventID;
    }
    
}
