<?php
/**
 * *************************************************************************
 * *                             events                                 **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  events                                                  **
 * @name        events                                                  **
 * @copyright   Glendon ITS                                               **
 * @link        http://www.glendon.yorku.ca/its                           **
 * @author      Patrick Thibaudeau                                        **
 * @author      Johanna Parrales                                          **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */

namespace local_events;

/**
 * Description of export
 *
 * @author patrick
 */
class Export {

    /**
     * 
     * @global \stdClass $CFG
     * @global \moodle_database $DB
     * @global \stdClass $USER
     * @param int $startdate
     * @param int $enddate
     */
    public static function yorkUniversityFormat($startdate = 0, $enddate = 0, $coming_going = 1) {
        global $CFG, $DB, $USER;

        require_once($CFG->dirroot . '/lib/phpexcel/PHPExcel.php');

        if ($startdate == 0 && $enddate == 0) {
            $results = $DB->get_records('local_events', ['coming_going' => $coming_going]);
        } else {
            $sql = "SELECT * FROM {local_events} WHERE coming_going = ? AND (timecreated BETWEEN ? AND ?)";
            $results = $DB->get_records_sql($sql, [$coming_going, $startdate, $enddate]);
        }

        $context = \context_system::instance();

        date_default_timezone_set('America/Toronto');
        if (PHP_SAPI == 'cli')
            die('This example should only be run from a Web Browser');

        $objPHPExcel = new \PHPExcel();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator(fullname($USER))
                ->setLastModifiedBy(fullname($USER))
                ->setTitle("")
                ->setSubject("BLM")
                ->setDescription("Template")
                ->setKeywords("BLM")
                ->setCategory("");

        // Add top  line
        $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A1', 'STUDENT NUMBER')
                ->setCellValue('B1', 'SURNAME')
                ->setCellValue('C1', 'FIRSTNAME')
                ->setCellValue('D1', 'SECONDNAME')
                ->setCellValue('E1', 'SALUTATION')
                ->setCellValue('F1', 'DATEOFBIRTH (YYYYMMDD)')
                ->setCellValue('G1', 'GENDER')
                ->setCellValue('H1', 'FIRSTLANGUAGE')
                ->setCellValue('I1', 'STATUSINCANADA')
                ->setCellValue('J1', 'COUNTRYOFBIRTH')
                ->setCellValue('K1', 'CITIZENSHIP')
                ->setCellValue('L1', 'MARITALSTATUS')
                ->setCellValue('M1', 'LANGUAGEOFCORRESPONDENCE')
                ->setCellValue('N1', 'DATEOFENTRY')
                ->setCellValue('O1', 'SIN NUMBER')
                ->setCellValue('P1', 'OEN NUMBER')
                ->setCellValue('Q1', 'PREVIOUSSISID')
                ->setCellValue('R1', 'CELLPHONE')
                ->setCellValue('S1', 'EMAIL')
                ->setCellValue('T1', 'Street Address')
                ->setCellValue('U1', 'CITY')
                ->setCellValue('V1', 'Canada/USA Prov/State')
                ->setCellValue('W1', 'Intl Province/State')
                ->setCellValue('X1', 'COUNTRY')
                ->setCellValue('Y1', 'POSTAL/ZIP CODE')
                ->setCellValue('Z1', 'HOME TELEPHONE')
                ->setCellValue('AA1', 'PROGID NUMBER')
                ->setCellValue('AB1', 'APPLICATIONDATE')
                ->setCellValue('AC1', 'DATERECEIVED')
                ->setCellValue('AD1', 'BASIS')
                ->setCellValue('AE1', 'STUDYLEVEL')
                ->setCellValue('AF1', 'ACTIVITYLEVEL')
                ->setCellValue('AG1', 'COHORT')
                ->setCellValue('AH1', 'STARTYEAR')
                ->setCellValue('AI1', 'STARTTERM')
                ->setCellValue('AJ1', '(Study Period)')
                ->setCellValue('AK1', 'HOME UNIVERSITY')
                ->setCellValue('AL1', 'COUNTRY')
                ->setCellValue('AM1', 'YU events CODE')
                ->setCellValue('AN1', 'PLANNED FIELD OF STUDY - CHOICE 1')
                ->setCellValue('AO1', 'PLANNED FIELD OF STUDY - CHOICE 2')
                ->setCellValue('AP1', 'PLANNED FIELD OF STUDY - CHOICE 3')
                ->setCellValue('AQ1', 'APPLIED FOR YORK INTERNATIONAL PROGRAM?')
                ->setCellValue('AR1', 'CURRENT SPECIALIZATION/MAJOR')
                ->setCellValue('AS1', 'CURRENT MINOR')
                ->setCellValue('AT1', 'CURRENT YEAR OF STUDY')
                ->setCellValue('AU1', 'AVERAGE GPA')
                ->setCellValue('AV1', 'READING IN FRENCH')
                ->setCellValue('AW1', 'WRITING IN FRENCH')
                ->setCellValue('AX1', 'SPEAKING IN FRENCH')
                ->setCellValue('AY1', 'COMPREHENSION IN FRENCH')
                ->setCellValue('AZ1', 'READING IN ENGLISH')
                ->setCellValue('BA1', 'WRITING IN ENGLISH')
                ->setCellValue('BB1', 'SPEAKING IN ENGLISH')
                ->setCellValue('BC1', 'COMPREHENSION IN ENGLISH')
                ->setCellValue('BD1', 'READING IN SPANISH')
                ->setCellValue('BE1', 'WRITING IN SPANISH')
                ->setCellValue('BF1', 'SPEAKING IN SPANISH')
                ->setCellValue('BG1', 'COMPREHENSION IN SPANISH')
                ->setCellValue('BH1', 'LANGUAGE TRAINING');

        $i = 2;

        foreach ($results as $r) {
            $user = $DB->get_record('user', ['id' => $r->userid]);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('A' . $i,
                    "$r->sisid");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('B' . $i,
                    "$user->lastname");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('C' . $i,
                    "$user->firstname");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('D' . $i,
                    "$user->middlename");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $i,
                    "$r->salutation");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $i,
                    date('Ymd', $r->dob));
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('G' . $i,
                    "$r->gender");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('H' . $i,
                    "$r->first_language");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('I' . $i,
                    "$r->status_in_canada");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('J' . $i, "");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('K' . $i,
                    "$r->citizenship");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('L' . $i, "");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('M' . $i,
                    "$r->language_of_correspondence");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('N' . $i, "");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('O' . $i, "");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('P' . $i, "");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Q' . $i,
                    "$r->previous_sisid");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('R' . $i,
                    "$r->cellphone");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('S' . $i,
                    "$user->email");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('T' . $i,
                    "$r->address");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('U' . $i,
                    "$r->city");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('V' . $i,
                    "$r->province");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('W' . $i,
                    "$r->state_province_region");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('X' . $i,
                    "$r->country");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Y' . $i,
                    "$r->postal_zip");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('Z' . $i,
                    "$r->home_telephone");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AA' . $i,
                    "$r->progid");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AB' . $i,
                    date('Ymd', $r->application_date));
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AC' . $i,
                    date('Ymd', $r->date_received));
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AD' . $i,
                    "$r->basis");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AE' . $i,
                    "$r->study_level");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AF' . $i,
                    "$r->activity_level");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AG' . $i, "");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AH' . $i,
                    "$r->start_year");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AI' . $i,
                    "$r->start_term");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AJ' . $i,
                    "$r->study_period");
            $yuCodes = \local_events\Base::getYueventsCode();
            $homeUniversity = $yuCodes[$r->home_university];
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AK' . $i,
                    "$homeUniversity");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AL' . $i,
                    "$r->university_country");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AM' . $i,
                    "$r->yu_events_code");
            
            $planned_field_of_study = json_decode($r->planned_field_of_study);
            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AN' . $i,
                    "$planned_field_of_study[0]");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AO' . $i,
                    "$planned_field_of_study[1]");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AP' . $i,
                    "$planned_field_of_study[2]");
            
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AQ' . $i,
                    "$r->york_international_program");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AR' . $i,
                    "$r->specialization_major");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AS' . $i,
                    "$r->minor");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AT' . $i,
                    "$r->current_year_of_study");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AU' . $i,
                    "$r->gpa");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AV' . $i,
                    "$r->reading_in_french");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AW' . $i,
                    "$r->writing_in_french");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AX' . $i,
                    "$r->speaking_in_french");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AY' . $i,
                    "$r->comprehension_in_french");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('AZ' . $i,
                    "$r->reading_in_english");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BA' . $i,
                    "$r->writing_in_english");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BB' . $i,
                    "$r->speaking_in_english");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BC' . $i,
                    "$r->comprehension_in_english");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BD' . $i,
                    "$r->reading_in_spanish");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BE' . $i,
                    "$r->writing_in_spanish");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BF' . $i,
                    "$r->speaking_in_spanish");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BG' . $i,
                    "$r->comprehension_in_spanish");
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('BH' . $i,
                    "$r->language_training");
            
            $i++;
        }

        // Rename worksheet
        $objPHPExcel->getActiveSheet()->setTitle("BLM");
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="BLM_Export_' . date('Ymd', time()) . '.xlsx"');
        header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');
// If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }

}
