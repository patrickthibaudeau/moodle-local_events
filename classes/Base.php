<?php

/**
 * *************************************************************************
 * *                             events                                 **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  events                                                  **
 * @name        events                                                  **
 * @copyright   Glendon ITS                                               **
 * @link        http://www.glendon.yorku.ca/its                           **
 * @author      Patrick Thibaudeau                                        **
 * @author      Johanna Parrales                                          **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */

namespace local_events;

class Base {

    /**
     * Creates the Moodle page header
     * @global \stdClass $CFG
     * @global \moodle_database $DB
     * @global \moodle_page $PAGE
     * @global \stdClass $SITE
     * @param string $url Current page url
     * @param string $pagetitle  Page title
     * @param string $pageheading Page heading (Note hard coded to site fullname)
     * @param array $context The page context (SYSTEM, COURSE, MODULE etc)
     * @return HTML Contains page information and loads all Javascript and CSS
     */
    public static function page($url, $pagetitle, $pageheading, $context, $pagelayout = 'admin') {
        global $CFG, $PAGE, $SITE;

        $stringman = get_string_manager();
        $strings = $stringman->load_component_strings('local_events', current_language());

        $PAGE->set_url($url);
        $PAGE->set_title($pagetitle);
        $PAGE->set_heading($pageheading);
        $PAGE->set_pagelayout('base');
        $PAGE->set_context($context);
        $PAGE->requires->css('/local/events/css/forms.css');
        $PAGE->requires->strings_for_js(array_keys($strings), 'local_events');
    }

    /**
     * Sets filemanager options
     * @global \stdClass $CFG
     * @param \stdClass $context
     * @param int $maxfiles
     * @return array
     */
    public static function getFileManagerOptions($context, $maxfiles = 1) {
        global $CFG;
        return array('subdirs' => 0, 'maxbytes' => $CFG->maxbytes, 'maxfiles' => $maxfiles);
    }

    /**
     * This function provides the javascript console.log function to print out php data to the console for debugging.
     * @param string $object
     */
    public static function consoleLog($object) {
        $html = '<script>';
        $html .= 'console.log("' . $object . '")';
        $html .= '</script>';

        echo $html;
    }

    public static function getEditorOptions($context) {
        global $CFG;
        return array('subdirs' => 1, 'maxbytes' => $CFG->maxbytes, 'maxfiles' => -1,
            'changeformat' => 1, 'context' => $context, 'noclean' => 1, 'trusttext' => true);
    }

    /**
     * Search users based on id, firstname or lastname. Used for jquery select2
     * @global stdobject $CFG
     * @global moodle_database $DB
     * @global session $USER
     * @param string $term Parameter is captured through URL
     * @return array $user JSON encoded array to be used with javascript
     */
    public static function search_users() {
        global $CFG, $DB, $USER;

        $term = optional_param('q', '', PARAM_RAW);
        $sql = "SELECT id,username,lastname,firstname,email,username, idnumber FROM {user} WHERE ";

        if (is_numeric($term)) {
            $sql = $sql . "idnumber LIKE '%$term%' ";
        } elseif (strstr($term, ' ')) {
            $terms = explode(' ', $term);
            $sql = $sql . "firstname LIKE '%$terms[0]%' AND lastname LIKE '%$terms[1]%'";
        } else {
            $sql = $sql . "firstname LIKE '%$term%' OR lastname LIKE '%$term%'";
        }
        $sql = $sql . " ORDER BY lastname, firstname";
        $results = $DB->get_records_sql($sql);
        foreach ($results as $result) {
            $users[] = array('id' => $result->id, 'text' => $result->lastname . ' ' . $result->firstname . ', PPY: ' . $result->username . ', Email: ' . $result->email);
        }
        return json_encode($users);
    }

    /**
     * Returns a single user record based on
     * 1. user id
     * 2. user name
     * 3. id number
     * $id will always have precedence
     * If an id is provide as well as a username, id will be used.
     * if an id is provided as well as a username and idnumber, id will be used
     * if a username and an id number are provided, username will be used.
     * @global \moodle_database $DB
     * @param int $id
     * @param string $username
     * @param string $idnumber
     */
    public static function getUserRecord($id = 0, $userName = '', $idNumber = '') {
        global $DB;


        if ($id != 0) {
            return $DB->get_record('user', array('id' => $id));
        }

        if ($userName != '') {
            return $DB->get_record('user', array('username' => $userName));
        }

        if ($idNumber != '') {
            return $DB->get_record('user', array('idnumber' => $idNumber));
        }
    }

    /**
     * Returns ability for reading, writing, speaking, comprehension 
     * in the language
     * @return array
     */
    public static function getAbility() {
        $ability = [
            '' => get_string('select'),
            "Excellent" => "Excellent",
            "Good / Bon" => "Good / Bon",
            "Fair / Moyen" => "Fair / Moyen",
            "Poor / Bas" => "Poor / Bas",
            "Not applicable / Non applicable" => "Not applicable / Non applicable",
        ];

        return $ability;
    }

    /**
     * Returns array of majors
     * @return array
     */
    public static function getMajors() {
        $major = [
            '' => get_string('select'),
            "Biology" => "Biology",
            "Business Economics" => "Business Economics",
            "Canadian Studies" => "Canadian Studies",
            "Communications" => "Communications",
            "Drama Studies" => "Drama Studies",
            "Economics" => "Economics",
            "Education" => "Education",
            "English Studies" => "English Studies",
            "French Studies" => "French Studies",
            "Gender and Women’s Studies" => "Gender and Women’s Studies",
            "History" => "History",
            "International Studies" => "International Studies",
            "Linguistics & Language Studies" => "Linguistics & Language Studies",
            "Mathematics" => "Mathematics",
            "Philosophy" => "Philosophy",
            "Political Science" => "Political Science",
            "Psychology" => "Psychology",
            "Sexuality Studies" => "Sexuality Studies",
            "Sociology" => "Sociology",
            "Spanish Studies" => "Spanish Studies",
            "Translation" => "Translation",
            "Other" => "Other",
        ];

        return $major;
    }

    public static function getYueventsCode() {
        $yuevents = [
            '' => get_string('select'),
            'FRANCE' => '-----FRANCE-----',
            'FMCE' => 'MICEFA',
            'FUNN ' => 'UNIVERSITÉ NICE SOPHIA ANTIPOLIS',
            'FIDP' => 'SCIENCES PO (REIMS, POITIERS, MENTON, LE HAVRE, NANCY, DIJON)',
            'IEPR ' => 'SCIENCES PO RENNES',
            'FIPS' => 'SCIENCES PO STRASBOURG',
            'FIPG' => 'SCIENCES PO SAINT-GERMAIN-EN-LAYE',
            'FIPL' => 'SCIENCES PO LILLE',
            'FBUM' => 'SCIENCES PO BORDEAUX',
            'ROUE ' => 'UNIVERSITÉ DE ROUEN NORMANDIE',
            'FUCO' => 'UNIVERSITÉ CATHOLIQUE DE L\'OUEST, ANGERS',
            'PAR5 ' => 'SORBONNE UNIVERSITÉ, PARIS',
            'LYON' => 'EMLYON BUSINESS SCHOOL, SAINT-ÉTIENNE',
            'BELIQUE' => '-----BELIGIQUE-----',
            'ISTI ' => 'INSTITUT SUPÉRIEUR DE TRADUCTEURS ET INTERPRÈTES (ISTI) - UNIVERSITÉ LIBRE DE BRUXELLES',
            'BULB' => 'UNIVERSITÉ LIBRE DE BRUXELLES',
            'BUNL' => 'UNIVERSITÉ DE LIÈGE',
            'SUISSE' => '-----SUISSE-----',
            'SUDL' => 'UNIVERSITÉ DE LAUSANNE (UNIL)',
            'SUNG' => 'FACULTÉ DE TRADUCTION ET D\'INTERPRÉTATION - UNIVERSITÉ DE GENÈVE',
            'NETHERLANDS' => '-----NETHERLANDS-----',
            'NRAC' => 'UNIVERSITY COLLEGE ROOSEVELT',
            'SPAIN' => '-----SPAIN-----',
            'UAB' => 'UNIVERSITAT AUTÒNOMA DE BARCELONA',
            'MEXICO' => '-----MEXICO-----',
            'MUAP' => 'UNIVERSIDAD DE LAS AMERICAS PUEBLA',
            'MECM ' => 'EL COLEGIO DE MÉXICO',
            'CANADA' => '-----CANADA-----',
            'CUDM ' => 'UNIVERSITÉ DE MONTRÉAL',
            'LAMO' => 'UNIVERSITÉ LAVAL',
            'YU' => 'YORK UNIVERSITY'
        ];

        return $yuevents;
    }

    public static function getSalutations() {
        $salutation = [
            '' => get_string('select'),
            'Miss' => get_string('Miss', 'local_events'),
            'Mr' => get_string('Mr', 'local_events'),
            'Mrs' => get_string('Mrs', 'local_events'),
            'Ms' => get_string('Ms', 'local_events'),
            'Reverend' => get_string('Reverend', 'local_events'),
            'Captain' => get_string('Captain', 'local_events'),
            'Dr' => get_string('Dr', 'local_events'),
            'Fr' => get_string('Fr', 'local_events'),
            'M' => get_string('M', 'local_events'),
            'Mlle' => get_string('Mlle', 'local_events'),
            'Mme' => get_string('Mme', 'local_events'),
            'Sr' => get_string('Sr', 'local_events'),
            'Rabbi' => get_string('Rabbi', 'local_events'),
            'Brother' => get_string('Brother', 'local_events'),
            'Colonel' => get_string('Colonel', 'local_events'),
            'General' => get_string('General', 'local_events'),
            'Major' => get_string('Major', 'local_events'),
            'Lieutenant' => get_string('Lieutenant', 'local_events'),
            'Admiral' => get_string('Admiral', 'local_events'),
            'Sargeant' => get_string('Sargeant', 'local_events'),
            'Professor' => get_string('Professor', 'local_events'),
            'Corporal' => get_string('Corporal', 'local_events'),
            'Sir' => get_string('Sir', 'local_events')
        ];

        return $salutation;
    }

    /**
     * Returns current year of study array
     * @return array
     */
    public static function getCurrentYearOfStudy() {
        $cys = [
            '' => get_string('select'),
            1 => "1st year / 1re année",
            2 => "2nd year / 2nde année",
            3 => "3rd year / 3e année",
            4 => "4th year / 4e année",
            5 => "Master / Maîtrise",
            
        ];

        return $cys;
    }

    /**
     * Returns an array of Canadian citizenship status
     * @return array
     */
    public static function getCanadianStatus() {
        $status = [
            '' => get_string('select'),
            "Canadian / Canadien" => "Canadian / Canadien",
            "Landed Immigrant / Résident permanent" => "Landed Immigrant / Résident permanent",
            "Not reported / Non reporté" => "Not reported / Non reporté",
            "Other / Autre" => "Other / Autre",
            "Unknown-Not Canadian / Inconnu- Non canadien" => "Unknown-Not Canadian / Inconnu- Non canadien",
            "Visa" => "Visa",
        ];

        return $status;
    }

    /**
     * Returns an array with languages Required because they do not use language codes
     * @return array 
     */
    public static function getLanguages() {
        $language = [
            '' => get_string('select'),
            "English / Anglais" => "English / Anglais",
            "French / Francais" => "French / Francais",
            "Aboriginal Lang NIE" => "Aboriginal Lang NIE",
            "African Lang NIE" => "African Lang NIE",
            "Akan" => "Akan",
            "Algonquian Languages" => "Algonquian Languages",
            "Algonquin" => "Algonquin",
            "Amharic" => "Amharic",
            "Arabic" => "Arabic",
            "Armenian" => "Armenian",
            "Asiatic Lang NIE" => "Asiatic Lang NIE",
            "Assemese" => "Assemese",
            "Athapaskan Lang NIE" => "Athapaskan Lang NIE",
            "Attikamek" => "Attikamek",
            "Austro-Asiatic Lang" => "Austro-Asiatic Lang",
            "Azerbaijani" => "Azerbaijani",
            "Bantu Languages NIE" => "Bantu Languages NIE",
            "Bashkir" => "Bashkir",
            "Basque" => "Basque",
            "Bemba" => "Bemba",
            "Bengali" => "Bengali",
            "Berber" => "Berber",
            "Bhili" => "Bhili",
            "Bikol" => "Bikol",
            "Blackfoot" => "Blackfoot",
            "Bulgarian" => "Bulgarian",
            "Burmese" => "Burmese",
            "Buyi" => "Buyi",
            "Byelorussian" => "Byelorussian",
            "Cantonese" => "Cantonese",
            "Carrier" => "Carrier",
            "Catalan (Provencal)" => "Catalan (Provencal)",
            "Cebuano  (Visayan)" => "Cebuano  (Visayan)",
            "Celtic Lang NIE" => "Celtic Lang NIE",
            "Chichewa" => "Chichewa",
            "Chilcotin" => "Chilcotin",
            "Chinese" => "Chinese",
            "Chipewyan" => "Chipewyan",
            "Chuvash" => "Chuvash",
            "Cree" => "Cree",
            "Creoles" => "Creoles",
            "Croatian" => "Croatian",
            "Czech" => "Czech",
            "Dakota/Sioux" => "Dakota/Sioux",
            "Danish" => "Danish",
            "Dong" => "Dong",
            "Dravidian Lang NIE" => "Dravidian Lang NIE",
            "Dutch" => "Dutch",
            "Efik-Ibbio" => "Efik-Ibbio",
            "Eng Fr And Non-Offic" => "Eng Fr And Non-Offic",
            "English and French" => "English and French",
            "English And Non-Offi" => "English And Non-Offi",
            "Estonian" => "Estonian",
            "Farsi (Persian)" => "Farsi (Persian)",
            "Finnish" => "Finnish",
            "Flemish" => "Flemish",
            "French And Non-Offic" => "French And Non-Offic",
            "Frisian" => "Frisian",
            "Fula" => "Fula",
            "Gaelic Languages" => "Gaelic Languages",
            "Galician" => "Galician",
            "Ganda" => "Ganda",
            "Georgian" => "Georgian",
            "German" => "German",
            "Germanic Lang NIE" => "Germanic Lang NIE",
            "Gitksan" => "Gitksan",
            "Greek" => "Greek",
            "Guarani" => "Guarani",
            "Gujarati" => "Gujarati",
            "Haida" => "Haida",
            "Hakka" => "Hakka",
            "Hausa" => "Hausa",
            "Hebrew" => "Hebrew",
            "Hindi" => "Hindi",
            "Hungarian" => "Hungarian",
            "Iba" => "Iba",
            "Icelandic" => "Icelandic",
            "Ilocano" => "Ilocano",
            "Indo-Iranian Lan NIE" => "Indo-Iranian Lan NIE",
            "Indonesian" => "Indonesian",
            "Inuit" => "Inuit",
            "Inuktitut (Eskimo)" => "Inuktitut (Eskimo)",
            "Iroquoian Lang NIE" => "Iroquoian Lang NIE",
            "Italian" => "Italian",
            "Japanese" => "Japanese",
            "Javanese" => "Javanese",
            "Kannada" => "Kannada",
            "Kanuri" => "Kanuri",
            "Kashmiri" => "Kashmiri",
            "Kazkh" => "Kazkh",
            "Khalka" => "Khalka",
            "Khmer (Cambodian)" => "Khmer (Cambodian)",
            "Kikuya" => "Kikuya",
            "Kirundi" => "Kirundi",
            "Konkani" => "Konkani",
            "Korean" => "Korean",
            "Kurdish" => "Kurdish",
            "Kurukh" => "Kurukh",
            "Kusaiean" => "Kusaiean",
            "Kutchin-Gwich' in" => "Kutchin-Gwich' in",
            "Kutenai" => "Kutenai",
            "Lao" => "Lao",
            "Latin" => "Latin",
            "Latvian (Lettish)" => "Latvian (Lettish)",
            "Lingala" => "Lingala",
            "Lithuanian" => "Lithuanian",
            "Luba-Lulua" => "Luba-Lulua",
            "Luo" => "Luo",
            "" => "",
            "Macedonian" => "Macedonian",
            "Madurese" => "Madurese",
            "Malagasy" => "Malagasy",
            "Malayalam" => "Malayalam",
            "Malay-Bahasa" => "Malay-Bahasa",
            "Malayo-Polynesian La" => "Malayo-Polynesian La",
            "Malecite" => "Malecite",
            "Malinke-Bambara-Dyul" => "Malinke-Bambara-Dyul",
            "Maltese" => "Maltese",
            "Mandarin" => "Mandarin",
            "Marathi" => "Marathi",
            "Marshallese" => "Marshallese",
            "Mende" => "Mende",
            "Micmac" => "Micmac",
            "Min" => "Min",
            "Minankabau" => "Minankabau",
            "Mohawk" => "Mohawk",
            "Montagnais-Naskapi" => "Montagnais-Naskapi",
            "More" => "More",
            "Nepali" => "Nepali",
            "Niger-Congo Lang NIE" => "Niger-Congo Lang NIE",
            "Nishga" => "Nishga",
            "Nootka" => "Nootka",
            "North Slave (Hare)" => "North Slave (Hare)",
            "Norwegian" => "Norwegian",
            "Not Applicable (Inst" => "Not Applicable (Inst",
            "Not Reported" => "Not Reported",
            "Nyanja" => "Nyanja",
            "Ojibway" => "Ojibway",
            "Oji-Cree" => "Oji-Cree",
            "Oriya" => "Oriya",
            "Oromo (Galla)" => "Oromo (Galla)",
            "Other Languages" => "Other Languages",
            "Palauan" => "Palauan",
            "Pantay-Hillgaynon" => "Pantay-Hillgaynon",
            "Pashto" => "Pashto",
            "Persian (Farsi)" => "Persian (Farsi)",
            "Philipino" => "Philipino",
            "Pidgin" => "Pidgin",
            "Polish" => "Polish",
            "Ponapean" => "Ponapean",
            "Portuguese" => "Portuguese",
            "Punjabi" => "Punjabi",
            "Pushtu" => "Pushtu",
            "Romance Lang NIE" => "Romance Lang NIE",
            "Romanian" => "Romanian",
            "Ruanda" => "Ruanda",
            "Russian" => "Russian",
            "Salish Lang NIE" => "Salish Lang NIE",
            "Samar-Leyte" => "Samar-Leyte",
            "Samoan" => "Samoan",
            "Santali" => "Santali",
            "Semitic Lang NIE" => "Semitic Lang NIE",
            "Serbian" => "Serbian",
            "Serbo-Croatian" => "Serbo-Croatian",
            "Sesotho" => "Sesotho",
            "Setswana" => "Setswana",
            "Shona" => "Shona",
            "Shuswap" => "Shuswap",
            "Sindhi" => "Sindhi",
            "Sinhalese" => "Sinhalese",
            "Sino-Tibetan Lan NIE" => "Sino-Tibetan Lan NIE",
            "Siswati" => "Siswati",
            "Slavic Lang NIE" => "Slavic Lang NIE",
            "Slovak" => "Slovak",
            "Slovenian" => "Slovenian",
            "Somali" => "Somali",
            "Somalian" => "Somalian",
            "South Slave" => "South Slave",
            "Spanish" => "Spanish",
            "Sundanese" => "Sundanese",
            "Swahili" => "Swahili",
            "Swedish" => "Swedish",
            "Tagalog (Pilipino)" => "Tagalog (Pilipino)",
            "Tamil" => "Tamil",
            "Tatar" => "Tatar",
            "Telugu" => "Telugu",
            "Thai" => "Thai",
            "Thompson Ntlakapamux" => "Thompson Ntlakapamux",
            "Tibetan" => "Tibetan",
            "Tigringa" => "Tigringa",
            "Tlingit" => "Tlingit",
            "Toncan" => "Toncan",
            "Trukese" => "Trukese",
            "Tsimshian" => "Tsimshian",
            "Tulu" => "Tulu",
            "Turkic Lang NIE" => "Turkic Lang NIE",
            "Turkish" => "Turkish",
            "Turkmen" => "Turkmen",
            "Twi" => "Twi",
            "Uighur" => "Uighur",
            "Ukrainian" => "Ukrainian",
            "Ulithian" => "Ulithian",
            "Unknown" => "Unknown",
            "Urdu" => "Urdu",
            "Vietnamese" => "Vietnamese",
            "Wakashan Lang NIE" => "Wakashan Lang NIE",
            "Welsh" => "Welsh",
            "Wolof" => "Wolof",
            "Wu" => "Wu",
            "Xhosa" => "Xhosa",
            "Yapese" => "Yapese",
            "Yi" => "Yi",
            "Yiddish" => "Yiddish",
            "Yoruba" => "Yoruba",
            "Yugoslav" => "Yugoslav",
            "Zhuang" => "Zhuang",
            "Zulu" => "Zulu",
        ];

        return $language;
    }

    public static function printProgressBar($pageNumber, $numberOfPages = 9) {
        $percentage = 0;
        if ($numberOfPages == 0) {
            $html = 'Step ' . $pageNumber;
        } else {
            $percentage = floor(($pageNumber / $numberOfPages) * 100);
            $html = 'Step ' . $pageNumber . ' of ' . $numberOfPages;
        }
        $html .= '<div class="progress mb-3" style="height: 25px;">' . "\n";
        $html .= '  <div class="progress-bar progress-bar-striped progress-bar-animated"'
                . ' role="progressbar" style="width: ' . $percentage . '%" aria-valuenow="' . $pageNumber . '"'
                . ' aria-valuemin="0" aria-valuemax="6">' . $percentage . '%</div>' . "\n";
        $html .= '</div>' . "\n";

        return $html;
    }

    public static function getCountriesArray() {
        $countries = [
            '' => get_string('select'),
            'Afghanistan' => 'Afghanistan',
            'Albania' => 'Albania',
            'Algeria' => 'Algeria',
            'Andorra' => 'Andorra',
            'Angola' => 'Angola',
            'Antigua and Barbuda' => 'Antigua and Barbuda',
            'Argentina' => 'Argentina',
            'Armenia' => 'Armenia',
            'Australia' => 'Australia',
            'Austria' => 'Austria',
            'Azerbaijan' => 'Azerbaijan',
            'The Bahamas' => 'The Bahamas',
            'Bahrain' => 'Bahrain',
            'Bangladesh' => 'Bangladesh',
            'Barbados' => 'Barbados',
            'Belarus' => 'Belarus',
            'Belgium' => 'Belgium',
            'Belize' => 'Belize',
            'Benin' => 'Benin',
            'Bhutan' => 'Bhutan',
            'Bolivia' => 'Bolivia',
            'Bosnia and Herzegovina' => 'Bosnia and Herzegovina',
            'Botswana' => 'Botswana',
            'Brazil' => 'Brazil',
            'Brunei' => 'Brunei',
            'Bulgaria' => 'Bulgaria',
            'Burkina Faso' => 'Burkina Faso',
            'Burundi' => 'Burundi',
            'Advertisement' => 'Advertisement',
            'Cabo Verde' => 'Cabo Verde',
            'Cambodia' => 'Cambodia',
            'Cameroon' => 'Cameroon',
            'Canada' => 'Canada',
            'Central African Republic' => 'Central African Republic',
            'Chad' => 'Chad',
            'Chile' => 'Chile',
            'China' => 'China',
            'Colombia' => 'Colombia',
            'Comoros' => 'Comoros',
            'Congo, Democratic Republic of the' => 'Congo, Democratic Republic of the',
            'Congo, Republic of the' => 'Congo, Republic of the',
            'Costa Rica' => 'Costa Rica',
            'Côte d’Ivoire' => 'Côte d’Ivoire',
            'Croatia' => 'Croatia',
            'Cuba' => 'Cuba',
            'Cyprus' => 'Cyprus',
            'Czech Republic' => 'Czech Republic',
            'Denmark' => 'Denmark',
            'Djibouti' => 'Djibouti',
            'Dominica' => 'Dominica',
            'Dominican Republic' => 'Dominican Republic',
            'East Timor (Timor-Leste)' => 'East Timor (Timor-Leste)',
            'Ecuador' => 'Ecuador',
            'Egypt' => 'Egypt',
            'El Salvador' => 'El Salvador',
            'Equatorial Guinea' => 'Equatorial Guinea',
            'Eritrea' => 'Eritrea',
            'Estonia' => 'Estonia',
            'Ethiopia' => 'Ethiopia',
            'Advertisement' => 'Advertisement',
            'Fiji' => 'Fiji',
            'Finland' => 'Finland',
            'France' => 'France',
            'Gabon' => 'Gabon',
            'The Gambia' => 'The Gambia',
            'Georgia' => 'Georgia',
            'Germany' => 'Germany',
            'Ghana' => 'Ghana',
            'Greece' => 'Greece',
            'Grenada' => 'Grenada',
            'Guatemala' => 'Guatemala',
            'Guinea' => 'Guinea',
            'Guinea-Bissau' => 'Guinea-Bissau',
            'Guyana' => 'Guyana',
            'Haiti' => 'Haiti',
            'Honduras' => 'Honduras',
            'Hungary' => 'Hungary',
            'Iceland' => 'Iceland',
            'India' => 'India',
            'Indonesia' => 'Indonesia',
            'Iran' => 'Iran',
            'Iraq' => 'Iraq',
            'Ireland' => 'Ireland',
            'Israel' => 'Israel',
            'Italy' => 'Italy',
            'Jamaica' => 'Jamaica',
            'Japan' => 'Japan',
            'Jordan' => 'Jordan',
            'Kazakhstan' => 'Kazakhstan',
            'Kenya' => 'Kenya',
            'Kiribati' => 'Kiribati',
            'Korea, North' => 'Korea, North',
            'Korea, South' => 'Korea, South',
            'Kosovo' => 'Kosovo',
            'Kuwait' => 'Kuwait',
            'Kyrgyzstan' => 'Kyrgyzstan',
            'Laos' => 'Laos',
            'Latvia' => 'Latvia',
            'Lebanon' => 'Lebanon',
            'Lesotho' => 'Lesotho',
            'Liberia' => 'Liberia',
            'Libya' => 'Libya',
            'Liechtenstein' => 'Liechtenstein',
            'Lithuania' => 'Lithuania',
            'Luxembourg' => 'Luxembourg',
            'Macedonia' => 'Macedonia',
            'Madagascar' => 'Madagascar',
            'Malawi' => 'Malawi',
            'Malaysia' => 'Malaysia',
            'Maldives' => 'Maldives',
            'Mali' => 'Mali',
            'Malta' => 'Malta',
            'Marshall Islands' => 'Marshall Islands',
            'Mauritania' => 'Mauritania',
            'Mauritius' => 'Mauritius',
            'Mexico' => 'Mexico',
            'Micronesia, Federated States of' => 'Micronesia, Federated States of',
            'Moldova' => 'Moldova',
            'Monaco' => 'Monaco',
            'Mongolia' => 'Mongolia',
            'Montenegro' => 'Montenegro',
            'Morocco' => 'Morocco',
            'Mozambique' => 'Mozambique',
            'Myanmar (Burma)' => 'Myanmar (Burma)',
            'Namibia' => 'Namibia',
            'Nauru' => 'Nauru',
            'Nepal' => 'Nepal',
            'Netherlands' => 'Netherlands',
            'New Zealand' => 'New Zealand',
            'Nicaragua' => 'Nicaragua',
            'Niger' => 'Niger',
            'Nigeria' => 'Nigeria',
            'Norway' => 'Norway',
            'Oman' => 'Oman',
            'Pakistan' => 'Pakistan',
            'Palau' => 'Palau',
            'Panama' => 'Panama',
            'Papua New Guinea' => 'Papua New Guinea',
            'Paraguay' => 'Paraguay',
            'Peru' => 'Peru',
            'Philippines' => 'Philippines',
            'Poland' => 'Poland',
            'Portugal' => 'Portugal',
            'Qatar' => 'Qatar',
            'Romania' => 'Romania',
            'Russia' => 'Russia',
            'Rwanda' => 'Rwanda',
            'Saint Kitts and Nevis' => 'Saint Kitts and Nevis',
            'Saint Lucia' => 'Saint Lucia',
            'Saint Vincent and the Grenadines' => 'Saint Vincent and the Grenadines',
            'Samoa' => 'Samoa',
            'San Marino' => 'San Marino',
            'Sao Tome and Principe' => 'Sao Tome and Principe',
            'Saudi Arabia' => 'Saudi Arabia',
            'Senegal' => 'Senegal',
            'Serbia' => 'Serbia',
            'Seychelles' => 'Seychelles',
            'Sierra Leone' => 'Sierra Leone',
            'Singapore' => 'Singapore',
            'Slovakia' => 'Slovakia',
            'Slovenia' => 'Slovenia',
            'Solomon Islands' => 'Solomon Islands',
            'Somalia' => 'Somalia',
            'South Africa' => 'South Africa',
            'Spain' => 'Spain',
            'Sri Lanka' => 'Sri Lanka',
            'Sudan' => 'Sudan',
            'Sudan, South' => 'Sudan, South',
            'Suriname' => 'Suriname',
            'Swaziland' => 'Swaziland',
            'Sweden' => 'Sweden',
            'Switzerland' => 'Switzerland',
            'Syria' => 'Syria',
            'Taiwan' => 'Taiwan',
            'Tajikistan' => 'Tajikistan',
            'Tanzania' => 'Tanzania',
            'Thailand' => 'Thailand',
            'Togo' => 'Togo',
            'Tonga' => 'Tonga',
            'Trinidad and Tobago' => 'Trinidad and Tobago',
            'Tunisia' => 'Tunisia',
            'Turkey' => 'Turkey',
            'Turkmenistan' => 'Turkmenistan',
            'Tuvalu' => 'Tuvalu',
            'Uganda' => 'Uganda',
            'Ukraine' => 'Ukraine',
            'United Arab Emirates' => 'United Arab Emirates',
            'United Kingdom' => 'United Kingdom',
            'United States' => 'United States',
            'Uruguay' => 'Uruguay',
            'Uzbekistan' => 'Uzbekistan',
            'Vanuatu' => 'Vanuatu',
            'Vatican City' => 'Vatican City',
            'Venezuela' => 'Venezuela',
            'Vietnam' => 'Vietnam',
            'Yemen' => 'Yemen',
            'Zambia' => 'Zambia',
            'Zimbabwe' => 'Zimbabwe',
        ];

        return $countries;
    }

    public static function getProvincesSatesArray() {
        $provinces = [
            '' => get_string('select'),
            "CANADA" => "---------Canada------------",
            'Alberta' => 'Alberta',
            'British Columbia' => 'British Columbia',
            'Manitoba' => 'Manitoba',
            'New Brunswick' => 'New Brunswick',
            'Newfoundland and Labrador' => 'Newfoundland and Labrador',
            'Nova Scotia' => 'Nova Scotia',
            'Ontario' => 'Ontario',
            'Prince Edward Island' => 'Prince Edward Island',
            'Quebec' => 'Quebec',
            'Saskatchewan' => 'Saskatchewan',
            'Northwest Territories' => 'Northwest Territories',
            'Nunavut' => 'Nunavut',
            'Yukon' => 'Yukon',
            'USA' => ' ---------United States------------',
            'Alabama' => 'Alabama',
            'Alaska' => 'Alaska',
            'Arizona' => 'Arizona',
            'Arkansas' => 'Arkansas',
            'California' => 'California',
            'Colorado' => 'Colorado',
            'Connecticut' => 'Connecticut',
            'Delaware' => 'Delaware',
            'District Of Columbia' => 'District Of Columbia',
            'Florida' => 'Florida',
            'Georgia' => 'Georgia',
            'Hawaii' => 'Hawaii',
            'Idaho' => 'Idaho',
            'Illinois' => 'Illinois',
            'Indiana' => 'Indiana',
            'Iowa' => 'Iowa',
            'Kansas' => 'Kansas',
            'Kentucky' => 'Kentucky',
            'Louisiana' => 'Louisiana',
            'Maine' => 'Maine',
            'Maryland' => 'Maryland',
            'Massachusetts' => 'Massachusetts',
            'Michigan' => 'Michigan',
            'Minnesota' => 'Minnesota',
            'Mississippi' => 'Mississippi',
            'Missouri' => 'Missouri',
            'Montana' => 'Montana',
            'Nebraska' => 'Nebraska',
            'Nevada' => 'Nevada',
            'New Hampshire' => 'New Hampshire',
            'New Jersey' => 'New Jersey',
            'New Mexico' => 'New Mexico',
            'New York' => 'New York',
            'North Carolina' => 'North Carolina',
            'North Dakota' => 'North Dakota',
            'Ohio' => 'Ohio',
            'Oklahoma' => 'Oklahoma',
            'Oregon' => 'Oregon',
            'Pennsylvania' => 'Pennsylvania',
            'Rhode Island' => 'Rhode Island',
            'South Carolina' => 'South Carolina',
            'South Dakota' => 'South Dakota',
            'Tennessee' => 'Tennessee',
            'Texas' => 'Texas',
            'Utah' => 'Utah',
            'Vermont' => 'Vermont',
            'Virginia' => 'Virginia',
            'Washington' => 'Washington',
            'West Virginia' => 'West Virginia',
            'Wisconsin' => 'Wisconsin',
            'Wyoming' => 'Wyoming',
            'American Samoa' => 'American Samoa',
            'Guam' => 'Guam',
            'Northern Mariana Islands' => 'Northern Mariana Islands',
            'Puerto Rico' => 'Puerto Rico',
            'United States Minor Outlying Islands' => 'United States Minor Outlying Islands',
            'Virgin Islands' => 'Virgin Islands',
            'Armed Forces Americas' => 'Armed Forces Americas',
            'Armed Forces Pacific' => 'Armed Forces Pacific',
            'Armed Forces Others' => 'Armed Forces Others'
        ];

        return $provinces;
    }
    
    /* Provinces only */
    
    public static function getProvincesArray() {
        $provincesOnly = [
            '' => get_string('select'),
            "CANADA" => "---------Canada------------",
            'Alberta' => 'Alberta',
            'British Columbia' => 'British Columbia',
            'Manitoba' => 'Manitoba',
            'New Brunswick' => 'New Brunswick',
            'Newfoundland and Labrador' => 'Newfoundland and Labrador',
            'Nova Scotia' => 'Nova Scotia',
            'Ontario' => 'Ontario',
            'Prince Edward Island' => 'Prince Edward Island',
            'Quebec' => 'Quebec',
            'Saskatchewan' => 'Saskatchewan',
            'Northwest Territories' => 'Northwest Territories',
            'Nunavut' => 'Nunavut',
            'Yukon' => 'Yukon'           
        ];

        return $provincesOnly;
    }
    
    
    /**
     * Returns an array of Insurance Coverage
     * @return array
     */
//    public static function getCanadianStatus() {
//        $status = [
//            '' => get_string('select'),
//            "Canadian / Canadien" => "Canadian / Canadien",
//            "Landed Immigrant / Résident permanent" => "Landed Immigrant / Résident permanent",
//            "Not reported / Non reporté" => "Not reported / Non reporté",
//            "Other / Autre" => "Other / Autre",
//            "Unknown-Not Canadian / Inconnu- Non canadien" => "Unknown-Not Canadian / Inconnu- Non canadien",
//            "Visa" => "Visa",
//        ];
//
//        return $status;
//    }

    /**
     * 
     * @global \stdClass $CFG
     * @global \moodle_database $DB
     * @global \stdClass $USER
     * @param int $id - application id
     */
    public static function send_email_notification($id) {
        global $CFG, $DB, $USER;

        $events_user = $DB->get_record('user', ['email' => 'events@glendon.yorku.ca']);
        $subject = 'Nouvelle inscription / New registration';
        $email_body_en = 'The following student has registered:<br>'
                . 'Name: ' . fullname($USER) . '<br>'
                . 'SISID: ' . $USER->idnumber . '<br>'
                . 'View application: <a href="' . $CFG->wwwroot . '/local/events/admin/result.php?id=' . $id . '">' . $CFG->wwwroot . '/local/events/admin/result.php?id=' . $id . '</a>';
        $email_body_fr = 'L\'étudiant suivant s\'est inscrit:<br>'
                . 'Nom: ' . fullname($USER) . '<br>'
                . 'SISID: ' . $USER->idnumber . '<br>'
                . 'Voir son application: <a href="' . $CFG->wwwroot . '/local/events/admin/result.php?id=' . $id . '">' . $CFG->wwwroot . '/local/events/admin/result.php?id=' . $id . '</a>';
        $email_body = $email_body_fr . '<br><br>----------<br><br>' . $email_body_en;

        email_to_user($events_user, null, $subject, null, $email_body);

        //Email student
        $subject = 'Votre demande a été soumise / Your appllication has been submitted';
        $email_body_en = 'This email is to confirm that your application for the Glendon events program has been submitted. If you have any questions, please contact us at events@glendon.yorku.ca.';
        $email_body_fr = 'Ce courriel confirme que votre candidature au programme d\'échange de Glendon a bien été soumise. Si vous avez des questions, svp nous contacter à events@glendon.yorku.ca.';
        $email_body = $email_body_fr . '<br><br>----------<br><br>' . $email_body_en;

        email_to_user($USER, null, $subject, null, $email_body);
    }

    /**
     * 
     * @global \stdClass $CFG
     * @global \moodle_database $DB
     * @param int $starttime
     * @param int $endtime
     */
    public static function get_applications($starttime = null, $endtime = null, $coming_going = 1) {
        global $CFG, $DB;

        if ($starttime == null) {
            $sql = 'SELECT u.firstname, u.lastname, u.email, e.id, e.sisid, e.timesubmitted,'
                    . 'e.legal_agreement from mdl_kiosk_events e '
                    . 'inner join mdl_user u on e.userid = u.id '
                    . 'WHERE e.coming_going=?';
            $params = [$coming_going];
        } else {
            $sql = 'SELECT u.firstname, u.lastname, e.id, e.sisid, e.timesubmitted, '
                    . 'e. legal_agreement from mdl_kiosk_events e '
                    . 'inner join mdl_user u on e.userid = u.id '
                    . 'WHERE e.timecreated BETWEEN ? AND ? '
                    . 'AND coming_going=?';
            $params = [$starttime, $endtime, $coming_going];
        }



        $results = $DB->get_records_sql($sql, $params);

        return $results;
    }

    /**
     * Generates a password
     * Taken from https://www.phpjabbers.com/generate-a-random-password-with-php-php70.html
     * Modified by Patrick Thibaudeau
     * @param int $length - the length of the generated password
     * @param int $count - number of passwords to be generated
     * @param string $characters - types of characters to be used in the password
     * @return string
     */
    public static function randomPassword($length, $count, $characters) {

// define variables used within the function    
        $symbols = array();
        $passwords = array();
        $used_symbols = '';
        $pass = '';

// an array of different character types    
        $symbols["lower_case"] = 'abcdefghijklmnopqrstuvwxyz';
        $symbols["upper_case"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $symbols["numbers"] = '1234567890';
        $symbols["special_symbols"] = '!?~@#-_+<>[]{}';

        $characters = explode(",", $characters); // get characters types to be used for the passsword
        foreach ($characters as $key => $value) {
            $used_symbols .= $symbols[$value]; // build a string with all characters
        }
        $symbols_length = strlen($used_symbols) - 1; //strlen starts from 0 so to get number of characters deduct 1

        for ($p = 0; $p < $count; $p++) {
            $pass = '';
            for ($i = 0; $i < $length; $i++) {
                $n = rand(0, $symbols_length); // get a random character from the string with all characters
                $pass .= $used_symbols[$n]; // add the character to the password string
            }
            $passwords[] = $pass;
        }

        return $passwords; // return the generated password
    }

}
