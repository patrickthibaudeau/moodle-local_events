<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once($CFG->libdir . "/externallib.php");
require_once("$CFG->dirroot/config.php");
//require_once("$CFG->dirroot/local/events/classes/event.php");

class local_events_external extends external_api {
    //**************************** SEARCH USERS **********************

    /**
     * Return weather information.
     *
     * @return single_structure_description
     */
    public static function create_event_details() {
        $object = array(
            'id' => new external_value(PARAM_INT, 'id'),
        );
        return new external_single_structure($object);
    }

    /**
     * Returns description of method parameters
     * @return external_function_parameters
     */
    public static function create_event_parameters() {
        return new external_function_parameters(
                array(
            'name' => new external_value(PARAM_TEXT, 'Name'),
            'email' => new external_value(PARAM_TEXT, 'Email'),
            'eventtitle' => new external_value(PARAM_TEXT, 'Event Title'),
            'eventdate' => new external_value(PARAM_RAW, 'Event Date'),
            'starttime' => new external_value(PARAM_RAW, 'Start Time'),
            'endtime' => new external_value(PARAM_RAW, 'End Time'),
            'location' => new external_value(PARAM_TEXT, 'Location'),
            'reservedroom' => new external_value(PARAM_TEXT, 'Reserved Room'),
            'budgetnumber' => new external_value(PARAM_INT, 'Budget Number'),
            'maxattendance' => new external_value(PARAM_INT, 'Campus address'),
            'eventdescription' => new external_value(PARAM_TEXT, 'Event Description'),
            'deleted' => new external_value(PARAM_BOOL, 'Deleted'),
                )
        );
    }

    /**
     * Creates an event
     * @return Create event
     */
    public static function create_event($name,$email,$eventtile,$eventdate,$starttime,$endtime,$location,$reservedroom,
                                 $budgetnumber,$maxattendance,$eventdescription,$deleted)
                                 {
        global $USER, $DB;

        //Parameter validation
        //
        $data = array(
                    'name' => $name,
                    'email' => $email,
                    'eventtitle' => $eventtile,
                    'eventdate' => $eventdate,
                    'starttime' => $starttime,
                    'endtime' => $endtime,
                    'location' => $location,
                    'reservedroom' => $reservedroom,
                    'budgetnumber' => $budgetnumber,
                    'maxattendance' => $maxattendance,
                    'eventdescription' => $eventdescription,
                    'deleted' => $deleted
                        );
        //REQUIRED
        $params = self::validate_parameters(self::search_parameters(), $data
        );

        //Context validation
        //OPTIONAL but in most web service it should present
        $context = context_system::instance();
        self::validate_context($context);
        
        $newEvent = new \local_events\event($data);
        
        $newEvent->create_event($data);
        
        
        
//        print_object($items);        

        
    }

    /**
     * Returns description of method result value
     * @return external_description
     */
    public static function create_event_returns() {
        return new external_multiple_structure(self::create_event_details());
    }

}
