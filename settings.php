<?php
/**
 * *************************************************************************
 * *                             events                                 **
 * *************************************************************************
 * @package     local                                                     **
 * @subpackage  events                                                  **
 * @name        events                                                  **
 * @copyright   Glendon ITS                                               **
 * @link        http://www.glendon.yorku.ca/its                           **
 * @author      Patrick Thibaudeau                                        **
 * @author      Johanna Parrales                                          **
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later  **
 * *************************************************************************
 * ************************************************************************ */

defined('MOODLE_INTERNAL') || die;

if ($hassiteconfig) {
    
}